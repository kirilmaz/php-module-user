<?php
declare(strict_types=1);

namespace Kirilmaz\Modules\User\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class IsAuthenticated {
    public function handle(Request $request, Closure $next) {
        if (false === Session::exists('user') || null === Session::get('user')) {
            return redirect()
                ->route('login');
        }

        return $next($request);
    }
}
