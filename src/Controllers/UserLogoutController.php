<?php
declare(strict_types=1);

namespace Kirilmaz\Modules\User\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserLogoutController extends Controller {
    public function index(Request $request) {
        Session::flush();

        $request->type = 'normal';

        return $this->response($request, (object) [
            'redirect' => '/'
        ]);
    }
}