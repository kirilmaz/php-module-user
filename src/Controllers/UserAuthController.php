<?php
declare(strict_types=1);

namespace Kirilmaz\Modules\User\Controllers;

use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Kirilmaz\Helpers\EmailAddressToParts;
use Kirilmaz\Modules\User\Models\Company;
use Kirilmaz\Modules\User\Models\LoginDomainDatabase;
use Kirilmaz\Modules\User\Models\LoginDomainUser;
use Kirilmaz\Modules\User\Models\Person;
use Kirilmaz\Modules\User\Models\User;
use Kirilmaz\Modules\User\Models\UserPassword;
use Kirilmaz\Modules\User\Models\UserUsername;

class UserAuthController extends Controller {
    public function index(Request $request) {
        Session::put('ip', null);
        Session::put('domain', null);
        Session::put('user', null);
        Session::put('databases', null);
        Session::put('language', null);

        $isDomainSet = false;
        $loginDomain = (object) [];
        $databases = [];
        $requestData = (object) $request->only('username', 'password');

        if(false === property_exists($requestData, 'username')
            || false === property_exists($requestData, 'password')) {
            return $this->response($request, (object) [
                'response' => 'error',
                'detail' => 'Invalid request'
            ]);
        }

        if(empty($requestData->username) || empty($requestData->password)) {
            return $this->response($request, (object) [
                'response' => 'error',
                'detail' => 'Username or password cannot be empty'
            ]);
        }

        if ($email = EmailAddressToParts::get($requestData->username)) {
            $requestData->username = $email->username;
            $requestData->domain = $email->domain;
        }

        $username = UserUsername::where([
            'username' => $requestData->username,
        ])->first();

        if (null === $username) {
            // TODO: Block if too much request comes
            return $this->response($request, (object) [
                'response' => 'error',
                'detail' => 'Username not found'
            ]);
        }

        if ($username->confirmed === 0) {
            return $this->response($request, (object) [
                'response' => 'error',
                'detail' => 'Username has not been confirmed yet'
            ]);
        }

        if ($username->active === 0) {
            return $this->response($request, (object) [
                'response' => 'error',
                'detail' => 'Username is not active'
            ]);
        }

        if ($username->status === 0) {
            return $this->response($request, (object) [
                'response' => 'error',
                'detail' => 'Username is disabled'
            ]);
        }

        $user = User::where('uuid', $username->user_uuid)->first();

        if (null == $user) {
            return $this->response($request, (object) [
                'response' => 'error',
                'detail' => 'User not found'
            ]);
        }

        $password = UserPassword::where('user_uuid', $username->user_uuid)->first();

        if (null == $password) {
            return $this->response($request, (object) [
                'response' => 'error',
                'detail' => 'Username does not have a password'
            ]);
        }

        if (false === Hash::check($requestData->password, $password->password)) {
            return $this->response($request, (object) [
                'response' => 'error',
                'detail' => 'Invalid username and password combination'
            ]);
        }

        if (true === empty($user->id_card_type) || empty($user->id_card_uuid)) {
            return $this->response($request, (object) [
                'response' => 'error',
                'detail' => 'ID card setup error'
            ]);
        }

        $idCard = match ($user->id_card_type) {
            'person' => Person::where('uuid', $user->id_card_uuid)->first(),
            'company' => Company::where('uuid', $user->id_card_uuid)->first(),
        };

        $loginDomains = LoginDomainUser::select('login_domains.domain_id', 'login_domains.domain', 'login_domains.domain_uuid', 'login_domain_users.default')
            ->leftJoin('login_domains', 'login_domain_users.domain_uuid', '=', 'login_domains.domain_uuid')
            ->where(['user_uuid' => $user->uuid])
            ->get();

        if (null === $loginDomains) {
            return $this->response($request, (object) [
                'response' => 'error',
                'detail' => 'No login domain found'
            ]);
        }

        $loginDomainsArray = $loginDomains->toArray();

        if(0 === count($loginDomainsArray)) {
            return $this->response($request, (object) [
                'response' => 'error',
                'detail' => 'No setup found for user'
            ]);
        }

        # User is Trying to Connect Using Domain Name
        if (true === property_exists($requestData, 'domain') && 0 === count($databases)) {
            $loginDomain = array_filter($loginDomainsArray, function($domain) use ($requestData) {
                if($domain['domain'] === $requestData->domain) {
                    return $domain;
                }
            });

            if (0 === count($loginDomain)) {
                return $this->response($request, (object) [
                    'response' => 'error',
                    'detail' => 'No setup found for domain'
                ]);
            }

            $loginDomain = array_values($loginDomain);
            $loginDomain = (object) $loginDomain[0];
            $isDomainSet = true;
        }

        if(false === $isDomainSet && 1 === count($loginDomainsArray)) {
            $loginDomain = (object) $loginDomainsArray[0];
        }

        if(false === $isDomainSet && 1 < count($loginDomainsArray)) {
            $loginDomain = array_filter($loginDomainsArray, function($domain) {
                if($domain['default'] === 1) {
                    return $domain;
                }
            });

            if(0 === count($loginDomain)) {
                $loginDomain = (object) $loginDomainsArray[0];
            }

            if (true === is_array($loginDomain)) {
                $loginDomain = (object) array_values($loginDomain)[0];
            }
        }

        $databases = LoginDomainDatabase::select(
            'name', 'driver', 'host', 'port', 'database', 'username', 'password', 'collation'
        )
            ->where([
                'domain_uuid'=> $loginDomain->domain_uuid,
                'status' => 1
            ])->get()
            ->toArray();

        if (property_exists($loginDomain, 'domain')) {
            Session::put('domain', (object) (object) [
                'id' => $loginDomain->domain_id,
                'uuid' => $loginDomain->domain_uuid,
                'name' => $loginDomain->domain
            ]);
        }

        Session::put('user', (object) [
            'title' => $idCard->title_long ?? $idCard->fullname,
            ...$user->toArray()
        ]);

        Session::put('ip', null);
        Session::put('databases', $databases);
        Session::put('language', 'tr');

        session()->regenerate();


        $secret = md5(uniqid(date('YmdHis') . $request->ip(), true));
        $key = md5(uniqid($user->id_card_uuid, true));

        try {
            $key .= bin2hex(random_bytes(30));
        } catch (\Exception $e) {
            $key .= md5($secret);
        }

        $domain = $request->getHost();

        $payload = [
            'iss' => $domain,
            'aud' => $domain,
            'iat' => 1356999524,
            'nbf' => 1357000000,
            'key' => $key
        ];

        $expires = time() + 60 * 60 * 24;

        setcookie(
            'jwt',
            JWT::encode($payload, $secret, 'HS256'),
            [
                'expires' => $expires,
                'path' => '/',
                'domain' => $domain,
                'secure' => false,
                'httponly' => true,
                'samesite' => 'Lax',
            ]);

        Redis::hset($key, 'data', json_encode((object) [
            'secret' => $secret,
            'user' => (object) [
                'fullname' => $user->fullname
            ]
        ]));

        return $this->response($request, (object) [
            'response' => 'success',
            'detail' => 'Login success',
            'redirect' => '/'
        ]);
    }
}