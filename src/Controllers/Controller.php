<?php
declare(strict_types=1);

namespace Kirilmaz\Modules\User\Controllers;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController {
    public function response(Request $request, Object $response) {
        if (null !== $request->type && 'normal' === strtolower($request->type)) {
            if (true === property_exists($response, 'redirect')) {
                return redirect($response->redirect);
            }
            if (true === property_exists($response, 'detail')) {
                pd($response->detail, false);
            }
        }

        return $response;
    }
}