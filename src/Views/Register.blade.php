@extends('Layouts/Public')

@section('content')
    <form id="registerForm" method="post" action="/user/register">
        {{ csrf_field() }}
        <br><br>
        <img src="/kirilmaz/img/company.logo.png" class="logo company medium">
        <br><br>

        <div class="row">
            <label for="email">Email Address</label>
            <div>
                <input type="text" name="email" value="" required autocomplete="email" id="email" autofocus>
            </div>

            <div>
                <button type="submit">
                    Register
                </button>
            </div><br>
        </div>
    </form>
@endsection