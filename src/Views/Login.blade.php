@extends('Layouts/Public')

@section('head')
@endsection

@section('content')
    <div class="center">
        <form id="loginForm" method="POST" action="/user/login/{{ conf('module-user')->requestType }}">
        {{ csrf_field() }}
        <br><br>
        <img src="/kirilmaz/img/company.logo.png" class="logo company medium">
        <br><br>

        <div class="row">
            <label for="username">Username</label>
            <div>
                <input id="username" type="username" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                @error('username')
                <div>{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <label for="password">Password</label>
            <div>
                <input id="password" type="password" name="password" required autocomplete="current-password">

                @error('password')
                <div>{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div style="display: flex">
            <div style="margin-right: auto;">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                <label for="remember">
                    Remember Me
                </label>
            </div>
            <div>
                <a href="/user/forgot-password">Forgot Password</a>
            </div>
        </div><br>

        <div>
            <button type="submit" class="buttonAjax">
                Login
            </button>
        </div><br>

        <div class="loginForm"></div>
    </form>
    </div>
@endsection

@section('script')

@endsection