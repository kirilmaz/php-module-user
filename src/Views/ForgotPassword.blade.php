@extends('Layouts/Public')

@section('content')
    <form id="forgotPasswordForm" method="post" action="/user/forgot-password">
        {{ csrf_field() }}
        <br><br>
        <div>
            <img src="/kirilmaz/img/company.logo.png" class="logo company medium">
        </div>
        <br><br>

        <div class="row">
            <label for="email">Email Address</label>
            <div>
                <input type="text" name="email" value="" required autocomplete="email" id="email" autofocus>
            </div>

            <div>
                <button type="submit">
                    Send Renew Request
                </button>
            </div><br>
        </div>
    </form>
@endsection