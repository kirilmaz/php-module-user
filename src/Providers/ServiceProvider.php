<?php
declare(strict_types=1);

namespace Kirilmaz\Modules\User\Providers;

use Illuminate\Routing\Router;
use Kirilmaz\Modules\User\Middlewares\IsAuthenticated;

class ServiceProvider extends \Illuminate\Support\ServiceProvider {
    public function boot() {
        $this->loadRoutesFrom(__DIR__ . '/../Routes/Web.php');
        $this->loadViewsFrom(__DIR__ . '/../Views/', 'KirilmazUser');

        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('IsAuthenticated', IsAuthenticated::class);

        $this->publishes([
            __DIR__.'/../Public' => public_path('kirilmaz')
        ], 'public');

        $this->publishes([
            __DIR__.'/../Configs' => base_path('configs')
        ]);

        unset($router);
    }

    public function register() { }
}