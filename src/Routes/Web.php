<?php

Route::prefix('user')->middleware('web')->group(function () {
    Route::get('/', function () {
        return view('KirilmazUser::Detail');
    })
        ->middleware([\Kirilmaz\Modules\User\Middlewares\IsAuthenticated::class])
        ->name('user');

    Route::get('login', function () {
        return view('KirilmazUser::Login');
    })
        ->name('login');

    Route::post('login/{type}', [
        \Kirilmaz\Modules\User\Controllers\UserAuthController::class, 'index'
    ]);

    Route::get('logout', [
        \Kirilmaz\Modules\User\Controllers\UserLogoutController::class, 'index'
    ])
        ->name('logout');

    Route::get('register', function () {
        return view('KirilmazUser::Register');
    })
        ->name('register');

    Route::post('register', [
        \Kirilmaz\Modules\User\Controllers\UserRegisterController::class, 'index'
    ]);

    Route::get('forgot-password', function () {
        return view('KirilmazUser::ForgotPassword');
    })
        ->name('forgot-password');
});