<?php
declare(strict_types=1);

namespace Kirilmaz\Modules\User\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model {
    use HasFactory;

    protected $connection = 'core';
    public $timestamps = false;
}